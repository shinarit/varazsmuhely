const HTML_PRE = `
<!doctype html>
<html>
  <head>
    <title>Varázslatos!!!</title>
  <style>
#nameDiv {
  font-variant: small-caps;
  font-size: 10pt;
}
p {
  margin: 0;
  text-align: justify;
  text-justify: inter-word;
  font-size: 10pt;
}
table {
  font-size: 10pt;
}
#endP {
  margin-bottom: 6pt;
}
#centered {
  text-align:center;
}
tr:nth-child(4n) td,
tr:nth-child(4n + 1) td {
    background-color: #fff;
}

tr:nth-child(4n + 2) td,
tr:nth-child(4n + 3) td {
    background-color: #ccc;
}
  </style>
  </head>
  <body style="font-family: Calibri">
`

const HTML_POST = `
  </body>
</html>
`

const TABLE_PRE=`<p><table>
  <tr>
    <th>Varázslat neve</th>
    <th>Fok</th>
    <th>H.erő</th>
    <th>H.táv</th>
    <th>H.ter</th>
    <th>H.idő</th>
    <th>H.mech</th>
    <th>Mp</th>
    <th>Var.</th>
    <th>idő</th>
    <th>Ellenállás</th>
  </tr>
`

const TABLE_DIVINE_PRE=`<table>
  <tr>
    <th>Varázslat neve</th>
    <th>Fok</th>
    <th>Típus</th>
    <th>H.erő</th>
    <th>H.táv</th>
    <th>H.ter</th>
    <th>H.idő</th>
    <th>H.mech</th>
    <th>Mp</th>
    <th>Var.</th>
    <th>idő</th>
    <th>Ellenállás</th>
  </tr>
`

const TABLE_POST=`</table>`

/*
result[spellName] = {
  source: [category],
  sourceFok: [fok],
  aspects: spell.aspects,
  description: spell.description,
  ritual: spell.ritual,
  resist: spell.resist,
  type: spell.type
};
*/
//[['Erősség', 2, 0, 0], ['Távolság', 5, 0, -1], ['Terület', 6, 0, -1], ['Időtartam', 1, 0, 0], ['Időtartam', 1, 0, 0], ['Mechanizmus', 2, 2, 0]],
const formatNumber = (num) => {
  if (num < 0) {
    return `${num}`
  } else if (num > 0) {
    return `+${num}`
  } else {
    return ''
  }
}
function collectAspects(aspects, which) {
  return aspects.filter((aspect) => aspect[0] === which)
    .map((aspect) => `${aspect[1]}${formatNumber(aspect[2])}${formatNumber(aspect[3])}`).join(', ')
}
function collectErőAspects(aspects) {
  return aspects.filter((aspect) => aspect[0] === "Erősség")
    .map((aspect) => `${aspect[1]}E`).join(', ')
}
// name, mana, time,
function formatSpell(spell) {
  return `
<p><span id="nameDiv"><b>${spell.name}</b></span></p>
<p><i><b>Típus (fok)</b>:</i> ${spell.magicSubclass} (${spell.fok})</p>
<p><i><b>Mana pont</b>:</i> ${spell.mana}</p>
<p><i><b>Varázslás ideje</b>:</i> ${spell.time}</p>
<p><i><b>Erősség</b>:</i> ${collectErőAspects(spell.aspects)} [${collectAspects(spell.aspects, "Erősség")}]</p>
<p><i><b>Távolság</b>:</i> ${spell.distanceText} [${collectAspects(spell.aspects, "Távolság")}]</p>
<p><i><b>Terület</b>:</i> ${spell.areaText} [${collectAspects(spell.aspects, "Terület")}]</p>
<p><i><b>Időtartam</b>:</i> ${spell.durationText} [${collectAspects(spell.aspects, "Időtartam")}]</p>
<p><i><b>Mechanizmus</b>:</i> ${spell.mechanismText} [${collectAspects(spell.aspects, "Mechanizmus")}]</p>
<p><i><b>Ellenállás</b>:</i> ${spell.resist}</p>
<p id="endP">${spell.description}</p>
`
}

function formatTableRow(spell) {
  if (spell.god) {
    return `  <tr>
    <td>${spell.name}</td>
    <td id="centered"><b>${spell.fok}</b></td>
    <td id="centered"><b>${spell.ritual ? "Rituálé" : "Litánia"}</b></td>
    <td id="centered">${collectAspects(spell.aspects, "Erősség")}</td>
    <td id="centered">${collectAspects(spell.aspects, "Távolság")}</td>
    <td id="centered">${collectAspects(spell.aspects, "Terület")}</td>
    <td id="centered">${collectAspects(spell.aspects, "Időtartam")}</td>
    <td id="centered">${collectAspects(spell.aspects, "Mechanizmus")}</td>
    <td id="centered">${spell.mana}</td>
    <td id="centered"><b>${spell.time.split('(')[1].split(')')[0]}</b></td>
    <td id="centered">${spell.time.split('(')[0]}</td>
    <td id="centered">${spell.resist}</td>
  </tr>`
  } else {
    return `  <tr>
    <td>${spell.name}</td>
    <td id="centered"><b>${spell.fok}</b></td>
    <td id="centered">${collectAspects(spell.aspects, "Erősség")}</td>
    <td id="centered">${collectAspects(spell.aspects, "Távolság")}</td>
    <td id="centered">${collectAspects(spell.aspects, "Terület")}</td>
    <td id="centered">${collectAspects(spell.aspects, "Időtartam")}</td>
    <td id="centered">${collectAspects(spell.aspects, "Mechanizmus")}</td>
    <td id="centered">${spell.mana}</td>
    <td id="centered"><b>${spell.time.split('(')[1].split(')')[0]}</b></td>
    <td id="centered">${spell.time.split('(')[0]}</td>
    <td id="centered">${spell.resist}</td>
  </tr>`
  }
}

function formatTables(spellList) {
  let group = null
  let result = ''
  const preTable = (spellList[0].god ? TABLE_DIVINE_PRE : TABLE_PRE)
  for (const spell of spellList) {
    if (spell.magicSubclass !== group) {
      if (group !== null) {
        result += TABLE_POST
      }
      group = spell.magicSubclass
      result += `<p><i><b>${group}</b></i></p>\n${preTable}`
    }
    result += formatTableRow(spell)
  }
  result += TABLE_POST
  return result
}

export function GenerateHtml(spellList) {
  if (spellList.length === 0) {
    return HTML_PRE + ":(" + HTML_POST
  }
  return HTML_PRE +
    formatTables(spellList) +
    '<p style="page-break-before: always" />' +
    spellList.map((spell) => formatSpell(spell)).join('\n') +
    HTML_POST
}
