import Vue from 'vue'
import App from './App.vue'

import Storage from 'vue-ls';

Vue.config.productionTip = false
const storageOptions = {
  namespace: 'varazsmuhely_', // key prefix
  name: 'ls', // name variable Vue.[ls] or this.[$ls],
  storage: 'local', // storage name session, local, memory
}
Vue.use(Storage, storageOptions);

new Vue({
  render: function (h) { return h(App) },
}).$mount('#app')
