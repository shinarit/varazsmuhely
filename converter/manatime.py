import math

ManaTable = {
  1: 0,
  2: 1,
  3: 3,
  4: 6,
  5: 10,
  6: 15,
  7: 21,
  8: 28,
  9: 36,
  10: 45,
  11: 55,
  12: 66,
  13: 78,
}

TimeCostModifier = -2

def manaCost(erő, táv, terület, idő, mech, rituálé):
  result = sum(map(lambda x: ManaTable[min(max(x, 1), 13)], sorted(erő + táv + terület + idő + mech, reverse=True)[0:5]))
  if rituálé: return math.floor(result * 2 / 3)
  return result

def timeCost(erő, táv, terület, idő, mech):
  return sum(map(lambda x: min(max(x, 1), 13) + TimeCostModifier, (max(erő), max(táv), max(terület), max(idő), max(mech))))

def textTime(tC, rituálé):
  multiplier = (3 if rituálé else 1)
  if tC <= 10:
    return "%d CS" % (max(1, tC) * multiplier)
  if tC <= 20:
    return "%d perc" % ((tC - 10) * multiplier)
  if tC <= 30:
    return "%d óra" % ((tC - 20) * multiplier)
  return "%d nap" % ((tC-30) * multiplier)

