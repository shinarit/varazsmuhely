from enum import Enum

class Struct:
  def __init__(self, **entries):
    self.__dict__.update(entries)

class DirectInterpreter:
  def __init__(self, idx):
    self.idx = idx
  
  def lastIdx(self):
    return self.idx
  
  def interpret(self, line):
    return line[self.idx]

class BoolInterpreter:
  def __init__(self, idx):
    self.idx = idx
  
  def lastIdx(self):
    return self.idx
  
  def interpret(self, line):
    return line[self.idx] == '1'

class ListInterpreter:
  def __init__(self, idx, len):
    self.idx = idx
    self.len = len
  
  def lastIdx(self):
    return self.idx + self.len - 1
  
  def interpret(self, line):
    return list(map(int, filter(bool, line[self.idx:self.idx + self.len])))

class Idx:
  Név = DirectInterpreter(0)
  Fok = DirectInterpreter(Név.lastIdx() + 1)
  Kaszt = DirectInterpreter(Fok.lastIdx() + 1)
  Forma = DirectInterpreter(Kaszt.lastIdx() + 1)
  Típus = DirectInterpreter(Forma.lastIdx() + 1)
  Rituálé = BoolInterpreter(Típus.lastIdx() + 1)
  Litánia = BoolInterpreter(Rituálé.lastIdx() + 1)
  É = BoolInterpreter(Litánia.lastIdx() + 1)
  H = BoolInterpreter(É.lastIdx() + 1)
  L = BoolInterpreter(H.lastIdx() + 1)
  T = BoolInterpreter(L.lastIdx() + 1)
  Isten = DirectInterpreter(T.lastIdx() + 1)
  Erő = ListInterpreter(Isten.lastIdx() + 1, 2)
  Táv = ListInterpreter(Erő.lastIdx() + 1, 2)
  TávSzöveg = DirectInterpreter(Táv.lastIdx() + 1)
  Terület = ListInterpreter(TávSzöveg.lastIdx() + 1, 2)
  TerületSzöveg = DirectInterpreter(Terület.lastIdx() + 1)
  Idő = ListInterpreter(TerületSzöveg.lastIdx() + 1, 2)
  IdőSzöveg = DirectInterpreter(Idő.lastIdx() + 1)
  Mechanizmus = ListInterpreter(IdőSzöveg.lastIdx() + 1, 4)
  MechanizmusSzöveg = DirectInterpreter(Mechanizmus.lastIdx() + 1)
  Kontrollált = BoolInterpreter(MechanizmusSzöveg.lastIdx() + 1)
  Irányított = BoolInterpreter(Kontrollált.lastIdx() + 1)
  Leplezett = BoolInterpreter(Irányított.lastIdx() + 2)
  Fenntartott = BoolInterpreter(Leplezett.lastIdx() + 1)
  Ismétlődő = BoolInterpreter(Fenntartott.lastIdx() + 1)
  KövetelménySzöveg = DirectInterpreter(Ismétlődő.lastIdx() + 1)
  KövetelményErősség = DirectInterpreter(KövetelménySzöveg.lastIdx()  + 1)
  KövetelményTávolság = DirectInterpreter(KövetelményErősség.lastIdx()  + 1)
  KövetelményTerület = DirectInterpreter(KövetelményTávolság.lastIdx()  + 1)
  KövetelményIdő = DirectInterpreter(KövetelményTerület.lastIdx()  + 1)
  KövetelményMechanizmus = DirectInterpreter(KövetelményIdő.lastIdx()  + 1)
  Ellenállás = DirectInterpreter(KövetelményMechanizmus.lastIdx() + 1)
  Leírás = DirectInterpreter(Ellenállás.lastIdx() + 1)

def arrayToSpell(array):
  return Struct(**{(key[0].lower() + key[1:] if len(key) > 1 else key):getattr(Idx, key).interpret(array) for key in Idx.__dict__ if not key.startswith('__')})

def lineToSpell(line):
  return arrayToSpell(line.split('|'))

