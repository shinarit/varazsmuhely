import sys
from collections import defaultdict

import manatime
from lineToSpell import lineToSpell

class Struct:
  def __init__(self, **entries):
    self.__dict__.update(entries)

HTML_PRE = """
<!doctype html>
<html>
  <head>
    <title>Varázslatos!!!</title>
  <style>
#nameDiv {
  font-variant: small-caps;
  font-size: 10pt;
}
p {
  margin: 0;
  text-align: justify;
  text-justify: inter-word;
  font-size: 10pt;
}
table {
  font-size: 10pt;
}
#endP {
  margin-bottom: 6pt;
}
#centered {
  text-align:center;
}
tr:nth-child(4n) td,
tr:nth-child(4n + 1) td {
    background-color: #fff;
}

tr:nth-child(4n + 2) td,
tr:nth-child(4n + 3) td {
    background-color: #ccc;
}
  </style>
  </head>
  <body style="font-family: Calibri">
"""

HTML_POST = """
  </body>
</html>
"""

GENERIC_SPELL_TEMPLATE = """
<p><span id="nameDiv"><b>%s</b></span></p>
<p><i><b>Típus (fok)</b>:</i> %s (%s)</p>
<p><i><b>Mana pont</b>:</i> %d</p>
<p><i><b>Varázslás ideje</b>:</i> %s (%d)</p>
<p><i><b>Erősség</b>:</i> %s [%s]</p>
<p><i><b>Távolság</b>:</i> %s [%s]</p>
<p><i><b>Terület</b>:</i> %s [%s]</p>
<p><i><b>Időtartam</b>:</i> %s [%s]</p>
<p><i><b>Mechanizmus</b>:</i> %s [%s]</p>
<p><i><b>Ellenállás</b>:</i> %s</p>
<p id="endP">%s</p>
"""

TABLE_PRE="""<p><table>
  <tr>
    <th>Varázslat neve</th>
    <th>Fok</th>
    <th>H.erő</th>
    <th>H.táv</th>
    <th>H.ter</th>
    <th>H.idő</th>
    <th>H.mech</th>
    <th>Mp</th>
    <th>Var.</th>
    <th>idő</th>
    <th>Ellenállás</th>
  </tr>
"""

TABLE_DIVINE_PRE="""<table>
  <tr>
    <th>Varázslat neve</th>
    <th>Fok</th>
    <th>Szféra</th>
    <th>Típus</th>
    <th>H.erő</th>
    <th>H.táv</th>
    <th>H.ter</th>
    <th>H.idő</th>
    <th>H.mech</th>
    <th>Mp</th>
    <th>Var.</th>
    <th>idő</th>
    <th>Ellenállás</th>
  </tr>
"""

TABLE_POST="""</table>"""

TABLE_ROW_TEMPLATE="""  <tr>
    <td>%s</td>
    <td id="centered"><b>%s</b></td>
    <td id="centered">%s</td>
    <td id="centered">%s</td>
    <td id="centered">%s</td>
    <td id="centered">%s</td>
    <td id="centered">%s</td>
    <td id="centered"><b>%d</b></td>
    <td id="centered">%d</td>
    <td id="centered">%s</td>
    <td>%s</td>
  </tr>"""

TABLE_ROW_DIVINE_TEMPLATE="""  <tr>
    <td>%s</td>
    <td id="centered"><b>%s</b></td>
    <td id="centered"><b>%s</b></td>
    <td id="centered">%s</td>
    <td id="centered">%s</td>
    <td id="centered">%s</td>
    <td id="centered">%s</td>
    <td id="centered">%s</td>
    <td id="centered">%s</td>
    <td id="centered"><b>%d</b></td>
    <td id="centered">%d</td>
    <td id="centered">%s</td>
    <td>%s</td>
  </tr>"""

def getModifiers(leplezett, fenntartott, kontrollált, irányított, ismétlődő):
  erőModifiers = []
  távModifiers = []
  területModifiers = []
  időModifiers = []
  mechanizmusModifiers = []
  
  if leplezett:
    erőModifiers.append(1)
    távModifiers.append(1)
    területModifiers.append(1)
    időModifiers.append(1)
    mechanizmusModifiers.append(1)
  if fenntartott:
    időModifiers.append(-1)
  if kontrollált:
    mechanizmusModifiers.append(1)
  if irányított:
    mechanizmusModifiers.append(-2)
  if ismétlődő:
    erőModifiers.append(1)
    távModifiers.append(1)
    területModifiers.append(1)
    időModifiers.append(1)
    mechanizmusModifiers.append(1)
  
  return Struct(**{
    "erő": erőModifiers,
    "táv": távModifiers,
    "terület": területModifiers,
    "idő": időModifiers,
    "mechanizmus": mechanizmusModifiers,
  })

def getComponents(spell):
  return Struct(**{
    "erő": [-int(spell.követelményErősség)] if spell.követelményErősség else [],
    "táv": [-int(spell.követelményTávolság)] if spell.követelményTávolság else [],
    "terület": [-int(spell.követelményTerület)] if spell.követelményTerület else [],
    "idő": [-int(spell.követelményIdő)] if spell.követelményIdő else [],
    "mechanizmus": [-int(spell.követelményMechanizmus)] if spell.követelményMechanizmus else [],
  })

def manaCost(spell):
  modifiers = getModifiers(spell.leplezett, spell.fenntartott, spell.kontrollált, spell.irányított, spell.ismétlődő)
  components = getComponents(spell)
  return manatime.manaCost(
    [x + sum(modifiers.erő) + (components.erő[idx] if len(components.erő) > idx else 0) for idx, x in enumerate(spell.erő)],
    [x + sum(modifiers.táv) + (components.táv[idx] if len(components.táv) > idx else 0) for idx, x in enumerate(spell.táv)],
    [x + sum(modifiers.terület) + (components.terület[idx] if len(components.terület) > idx else 0) for idx, x in enumerate(spell.terület)],
    [x + sum(modifiers.idő) + (components.idő[idx] if len(components.idő) > idx else 0) for idx, x in enumerate(spell.idő)],
    [x + sum(modifiers.mechanizmus) + (components.mechanizmus[idx] if len(components.mechanizmus) > idx else 0) for idx, x in enumerate(spell.mechanizmus)],
    spell.rituálé,
  )

def timeCost(spell):
  modifiers = getModifiers(spell.leplezett, spell.fenntartott, spell.kontrollált, spell.irányított, spell.ismétlődő)
  components = getComponents(spell)
  return manatime.timeCost(
    [x + sum(modifiers.erő) + (components.erő[idx] if len(components.erő) > idx else 0) for idx, x in enumerate(spell.erő)],
    [x + sum(modifiers.táv) + (components.táv[idx] if len(components.táv) > idx else 0) for idx, x in enumerate(spell.táv)],
    [x + sum(modifiers.terület) + (components.terület[idx] if len(components.terület) > idx else 0) for idx, x in enumerate(spell.terület)],
    [x + sum(modifiers.idő) + (components.idő[idx] if len(components.idő) > idx else 0) for idx, x in enumerate(spell.idő)],
    [x + sum(modifiers.mechanizmus) + (components.mechanizmus[idx] if len(components.mechanizmus) > idx else 0) for idx, x in enumerate(spell.mechanizmus)],
  )

def aspectTexts(spell):
  modifiers = getModifiers(spell.leplezett, spell.fenntartott, spell.kontrollált, spell.irányított, spell.ismétlődő)
  erőModText = ''.join(map(lambda x: '{0:+}'.format(x), modifiers.erő))
  távModText = ''.join(map(lambda x: '{0:+}'.format(x), modifiers.táv))
  területModText = ''.join(map(lambda x: '{0:+}'.format(x), modifiers.terület))
  időModText = ''.join(map(lambda x: '{0:+}'.format(x), modifiers.idő))
  mechanizmusModText = ''.join(map(lambda x: '{0:+}'.format(x), modifiers.mechanizmus))
  components = getComponents(spell)
  erőCompText = ['{0:+}'.format(x) for x in components.erő]
  távCompText = ['{0:+}'.format(x) for x in components.táv]
  területCompText = ['{0:+}'.format(x) for x in components.terület]
  időCompText = ['{0:+}'.format(x) for x in components.idő]
  mechanizmusCompText = ['{0:+}'.format(x) for x in components.mechanizmus]
  return Struct(**{
    "erő": ','.join((str(x) + erőModText + (erőCompText[idx] if len(erőCompText) > idx else "") for idx, x in enumerate(spell.erő))),
    "táv": ','.join((str(x) + távModText + (távCompText[idx] if len(távCompText) > idx else "") for idx, x in enumerate(spell.táv))),
    "terület": ','.join((str(x) + területModText + (területCompText[idx] if len(területCompText) > idx else "") for idx, x in enumerate(spell.terület))),
    "idő": ','.join((str(x) + időModText + (időCompText[idx] if len(időCompText) > idx else "") for idx, x in enumerate(spell.idő))),
    "mechanizmus": ','.join((str(x) + mechanizmusModText + (mechanizmusCompText[idx] if len(mechanizmusCompText) > idx else "") for idx, x in enumerate(spell.mechanizmus))),
  })

def spellToHtml(spell):
  calcMana = manaCost(spell)
  calcTime = timeCost(spell)
  textTime = manatime.textTime(calcTime, spell.rituálé)
  nameText = spell.név
  if spell.isten:
    nameText = "%s (%s)" % (nameText, spell.isten)
  formaText = spell.forma
  if spell.rituálé or spell.litánia:
    formaText = "%s %s %s" % (
      spell.típus,
      '/'.join(filter(lambda szféra: getattr(spell, szféra), "ÉHLT")),
      "Rituálé" if spell.rituálé else "Litánia")
  aspects = aspectTexts(spell)
#'{0:+} number'.format(1)
  return GENERIC_SPELL_TEMPLATE % (
    nameText,
    formaText,
    spell.fok,
    calcMana,
    textTime,
    calcTime,
    ', '.join(map(lambda e: '%dE' % e, spell.erő)),
    aspects.erő,
    spell.távSzöveg,
    aspects.táv,
    spell.területSzöveg,
    aspects.terület,
    spell.időSzöveg,
    aspects.idő,
    spell.mechanizmusSzöveg,
    aspects.mechanizmus,
    spell.ellenállás,
    spell.leírás
  )

def spellToRow(spell):
  aspects = aspectTexts(spell)
  calcMana = manaCost(spell)
  calcTime = timeCost(spell)
  textTime = manatime.textTime(calcTime, spell.rituálé)
  if spell.isten:
    return TABLE_ROW_DIVINE_TEMPLATE % (
      spell.név,
      spell.fok,
      ' '.join(filter(lambda szféra: getattr(spell, szféra), "ÉHLT")),
      "Rituálé" if spell.rituálé else "Litánia",
      aspects.erő,
      aspects.táv,
      aspects.terület,
      aspects.idő,
      aspects.mechanizmus,
      calcMana,
      calcTime,
      textTime,
      spell.ellenállás,
    )
  return TABLE_ROW_TEMPLATE % (
        spell.név,
        spell.fok,
        aspects.erő,
        aspects.táv,
        aspects.terület,
        aspects.idő,
        aspects.mechanizmus,
        calcMana,
        calcTime,
        textTime,
        spell.ellenállás,
      )

def fokNumerizer(fok):
  TABLE = {'If': 0, 'Af': 1, 'Kf': 2, 'Mf': 3, 'Lf': 4}
  return TABLE[fok]

def spellSort(spell):
  if spell.isten:
    return (spell.típus, fokNumerizer(spell.fok), manaCost(spell), spell.név)
  return (fokNumerizer(spell.fok), manaCost(spell), spell.név)

def spellsToHtml(lines):
  print(HTML_PRE)
  forms = defaultdict(list)
  for line in lines:
    spell = lineToSpell(line)
    if spell.isten:
      forms["%s (%s)" % (spell.forma, spell.isten)].append(spell)
    else:
      forms[spell.forma].append(spell)
  for form, spellz in forms.items():
    spellz = sorted(spellz, key=spellSort)
    print("<p><i><b>%s</b></i></p>" % form)
    if spellz[0].isten:
      print(TABLE_DIVINE_PRE)
    else:
      print(TABLE_PRE)
    for spell in spellz:
      print(spellToRow(spell))
    print(TABLE_POST)
    for spell in spellz:
      print(spellToHtml(spell))
  print(HTML_POST)

if __name__ == "__main__":
  spellsToHtml(filter(bool, open(sys.argv[1], 'r').read().split('\n')))
