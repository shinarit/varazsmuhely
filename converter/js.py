import sys
from collections import defaultdict
from lineToSpell import lineToSpell

class Struct:
  def __init__(self, **entries):
    self.__dict__.update(entries)

def formaTemplate():
  return {
    "If": {},
    "Af": {},
    "Kf": {},
    "Mf": {},
    "Lf": {},
  }

def kasztTemplate():
  return defaultdict(formaTemplate)

data = defaultdict(kasztTemplate)

LEVEL_ORDER = ["If", "Af", "Kf", "Mf", "Lf"]

def getModifiers(leplezett, fenntartott, kontrollált, irányított, ismétlődő):
  erőModifiers = 0
  távModifiers = 0
  területModifiers = 0
  időModifiers = 0
  mechanizmusModifiers = 0
  
  if leplezett:
    erőModifiers += 1
    távModifiers += 1
    területModifiers += 1
    időModifiers += 1
    mechanizmusModifiers += 1
  if fenntartott:
    időModifiers += -1
  if kontrollált:
    mechanizmusModifiers += 1
  if irányított:
    mechanizmusModifiers += -2
  if ismétlődő:
    erőModifiers += 1
    távModifiers += 1
    területModifiers += 1
    időModifiers += 1
    mechanizmusModifiers += 1
  
  return Struct(**{
    "Hatóerő": erőModifiers,
    "Hatótáv": távModifiers,
    "Hatóterület": területModifiers,
    "Hatóidő": időModifiers,
    "Hatásmechanizmus": mechanizmusModifiers,
  })

def getComponents(spell):
  return Struct(**{
    "erő": [-int(x) for x in spell.követelményErősség] + [0] * len(spell.erő),
    "táv": [-int(x) for x in spell.követelményTávolság] + [0] * len(spell.táv),
    "terület": [-int(x) for x in spell.követelményTerület] + [0] * len(spell.terület),
    "idő": [-int(x) for x in spell.követelményIdő] + [0] * len(spell.idő),
    "mechanizmus": [-int(x) for x in spell.követelményMechanizmus] + [0] * len(spell.mechanizmus),
  })

KasztConverter = {
  "Bárd": "Bárdmágia",
  "Boszorkány": "Boszorkánymágia",
  "Boszorkánymester": "Boszorkánymesteri mágia",
  "Tűzvarázsló": "Tűzvarázslói mágia",
  "Pap/paplovag": "Szakrális mágia",
}

def spellsToJs(spells):
  for line in spells:
    spell = lineToSpell(line)
    modifiers = getModifiers(spell.leplezett, spell.fenntartott, spell.kontrollált, spell.irányított, spell.ismétlődő)
    components = getComponents(spell)
    nameText = spell.név
    if spell.isten:
      nameText = "%s (%s)" % (nameText, spell.isten)
    formaTexts = (spell.forma,)
    if spell.rituálé or spell.litánia:
      formaTexts = ("%s %s fohászok" % (spell.típus, szféra) for szféra in filter(lambda szféra: getattr(spell, szféra[0]), ("Élet", "Halál", "Lélek", "Természet")))
    for forma in formaTexts:
      data[spell.kaszt][forma][spell.fok][nameText] = (
          [("Erősség", x, modifiers.Hatóerő, components.erő[idx]) for idx, x in enumerate(spell.erő)] +
          [("Távolság", x, modifiers.Hatótáv, components.táv[idx]) for idx, x in enumerate(spell.táv)] +
          [("Terület", x, modifiers.Hatóterület, components.terület[idx]) for idx, x in enumerate(spell.terület)] +
          [("Időtartam", x, modifiers.Hatóidő, components.idő[idx]) for idx, x in enumerate(spell.idő)] +
          [("Mechanizmus", x, modifiers.Hatásmechanizmus, components.mechanizmus[idx]) for idx, x in enumerate(spell.mechanizmus)],
        spell.isten, spell.leírás, spell.rituálé, spell.ellenállás,
        spell.távSzöveg, spell.területSzöveg, spell.időSzöveg, spell.mechanizmusSzöveg)
  
  print("[")
  for kaszt in data.keys():
    for forma in data[kaszt].keys():
      for level in LEVEL_ORDER:
        levelSpells = data[kaszt][forma][level]
        for spellName in levelSpells.keys():
          spell = data[kaszt][forma][level][spellName]
          print("  {")
          print("    name: '%s'," % spellName)
          print("    aspects: [%s]," % ", ".join(("[%s]" % ", ".join(map(repr, x)) for x in spell[0])))
          print("    fok: '%s'," % level)
          print("    magicSubclass: '%s'," % forma)
          print("    magicType: '%s'," % KasztConverter[kaszt])
          if spell[1] != "": print("    god: '%s'," % spell[1])
          print("    distanceText: '%s'," % spell[5])
          print("    areaText: '%s'," % spell[6])
          print("    durationText: '%s'," % spell[7])
          print("    mechanismText: '%s'," % spell[8])
          print("    description: '%s'," % spell[2])
          print("    resist: '%s'," % spell[4])
          if spell[3]: print("    ritual: true,")
          print("  },")
  print("]")


if __name__ == "__main__":
  spellsToJs(filter(bool, open(sys.argv[1], 'r').read().split('\n')))
